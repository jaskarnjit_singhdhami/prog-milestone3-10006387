﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaMenu
{
    class Program
    {
        public static void Main(string[] args)
        {
            client.clientinfo();
            Order.pizzas();
            Order.drinks();
            Order.Total();

        }
        public class client
        {
            public static string Name;
            public static int phoneNumber;

            public static void clientinfo()
            {
            infoStart:
                Console.Clear();
                Console.WriteLine("What is your name?");
                Name = Console.ReadLine();
                Console.WriteLine("What is your phone number(With out spaces)?");
                phoneNumber = int.Parse(Console.ReadLine());
                Console.Clear();
                Console.WriteLine("Is this the right details if yes press 1 to continue if no press 2?");
                Console.WriteLine($"{Name} { phoneNumber}");
                int redo = int.Parse(Console.ReadLine());
                if (redo == 2)
                {
                    goto infoStart;
                }
            }
        }
        public class Order
        {
            public static string p = "";
            public static int size = 0;
            public static string s = "";
            public static Double price = 0;

            public static string d = "";
            public static Double dprice = 0;

            public static List<Tuple<string, string>> Selection = new List<Tuple<string, string>>();
            public static List<Tuple<string>> types = new List<Tuple< string>>();

            public static void pizzas()
            {
                Console.Clear();
           
                
                
            pizzastart:
                Console.WriteLine("Choose your pizza by entring the number next to it");
                Console.WriteLine("1=Meat Lovers");
                Console.WriteLine("2=Pepperoni");
                Console.WriteLine("3=Ham & Cheese");
                Console.WriteLine("4=Chicken Supreme");
                Console.WriteLine("5=Veg Supreme");
                int pizza = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (pizza)
                {
                    case 1:
                        p = "Meat Lovers";
                        Console.WriteLine(p);
                        break;

                    case 2:
                        p = "Pepperoni";
                        Console.WriteLine(p);
                        break;

                    case 3:
                        p = "Ham & Cheese";
                        Console.WriteLine(p);
                        break;

                    case 4:
                        p = "Chicken Supreme";
                        Console.WriteLine(p);
                        break;

                    case 5:
                        p = "Veg Supreme";
                        Console.WriteLine(p);
                        break;

                    default:
                        goto pizzastart;

                }
                Console.Clear();
            sizestart:
                Console.WriteLine("What size would you like enter the number next to it");
                Console.WriteLine("1.Classic = 8 slices:Price = $9.00");
                Console.WriteLine("2.Large = 12 slices:Price = $13.00");
                size = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (size)
                {
                    case 1:
                        s = "Classic";
                        price = 9.00;
                        Console.WriteLine($"Your pizza size is {s} which is ${price}");
                        break;

                    case 2:
                        s = "Large";
                        price = 13.00;
                        Console.WriteLine($"Your pizza size is {s} which is ${price}");
                        break;

                    default:
                        goto sizestart;
                }
                
                Selection.Add(Tuple.Create(p, s));

                for (int i = 0; i < Selection.Count; i++)
                {
                    Console.WriteLine(Selection[i]);
                }

                Console.WriteLine(price++);

                Console.WriteLine("Would you like to order more pizza's, Yes=y No=n");
                string redo = Console.ReadLine();

                switch (redo)
                {
                    case "y":
                        goto pizzastart;

                    case "n":
                        
                        break;
                }
            }
            public static void drinks()
            {
                
               

                Console.Clear();
            drinkstart:
                Console.WriteLine("Choose your drink by entring the number next to it");
                Console.WriteLine("1=1.5 Pepsi $3.00");
                Console.WriteLine("2=1.5 Coke $3.50");
                Console.WriteLine("3=1.5 Sprite $3.50");
                Console.WriteLine("4=500ml Coke $2.50");
                Console.WriteLine("5=500ml Mountain Dew $2.00");
                int drink = int.Parse(Console.ReadLine());

                Console.Clear();
                switch (drink)
                {
                    case 1:
                        d = "1.5 Pepsi";
                        Console.WriteLine(d);
                        dprice = 3.00;
                        break;

                    case 2:
                        d = "1.5 Coke";
                        Console.WriteLine(d);
                        dprice = 3.50;
                        break;

                    case 3:
                        d = "1.5 Sprite";
                        Console.WriteLine(d);
                        dprice = 3.50;
                        break;

                    case 4:
                        d = "500ml Coke";
                        Console.WriteLine(d);
                        dprice = 2.50;
                        break;

                    case 5:
                        d = "500ml Mountain Dew";
                        Console.WriteLine(d);
                        dprice = 2.00;
                        break;

                    default:
                        goto drinkstart;

                }
                
                types.Add(Tuple.Create(d));

                for (int t = 0; t < types.Count; t++)
                {
                    Console.WriteLine(types[t]);
                }

                Console.WriteLine(dprice++);

                Console.WriteLine("Would you like to a another drink, Yes=y No=n");
                string re = Console.ReadLine();

                switch (re)
                {
                    case "y":
                        goto drinkstart;

                    case "n":
                        
                        break;
                }

            }
            public static void Total()
            {
                Console.Clear();

                Console.WriteLine($"Order for {client.Name} phone number {client.phoneNumber}");
                for (int i = 0; i < Selection.Count; i++)
                {
                    Console.WriteLine(Selection[i]);
                }
                for (int t = 0; t < types.Count; t++)
                {
                    Console.WriteLine(types[t]);
                }

                Double f = price + dprice;
               Console.WriteLine($"Total = ${f}");
                Console.WriteLine("Please enter your payment amount");
                double b = double.Parse(Console.ReadLine());

                double change = b - f;
                Console.WriteLine($"Your change is ${change}");

                
                Console.WriteLine("Thanks for your order");
            }
        }
    } 
}